import "./App.css";
import { useState, useEffect } from "react";

function App() {
  const [task, setTask] = useState("");
  const [taskList, setTaskList] = useState(null);

  useEffect(() => {
    if (localStorage.getItem("taskList") !== null) {
      const x = JSON.parse(localStorage.getItem("taskList"));
      setTaskList(x);
    } else {
      fetch("https://jsonplaceholder.typicode.com/users/1/todos")
        .then((response) => {
          if (response.ok) return response.json(); // Returns to then()
          return Promise.reject(response);
        })
        .then((data) => {
          setTaskList(data);
        })
        .catch((err) => {
          console.error(err); // Error
        });
    }
  }, []);

  useEffect(() => {
    if (taskList !== null) {
      localStorage.setItem("taskList", JSON.stringify(taskList));
    }
  }, [taskList]);

  if (taskList === null) {
    return <h1>LOADING</h1>;
  } else {
    return (
      <div className="App">
        <h1>Ma ToDoList (React)</h1>

        {/* AJOUT TÂCHE */}

        <input value={task} onChange={(e) => setTask(e.target.value)} />
        <button
          onClick={() => {
            const newTask = [
              ...taskList,
              { id: Date.now(), title: task, completed: false },
            ];
            setTaskList(newTask);
            // setTaskList([
            //   ...taskList,
            //   { id: Date.now(), title: task, completed: false },
            // ]);
            setTask("");
          }}
        >
          Ajouter
        </button>

        {/* LISTE DES TÂCHES */}

        <ul>
          {taskList.map((taskItem) => (
            <li key={taskItem.id}>
              {taskItem.completed ? (
                <span id="completed">✅</span>
              ) : (
                <span id="todo">❌</span>
              )}
              <input
                value={taskItem.title}
                onChange={(e) => {
                  setTaskList(
                    taskList.map((t) => {
                      if (t.id === taskItem.id) {
                        t.id = taskItem.id;
                        t.title = e.target.value;
                        t.completed = taskItem.completed;
                      }
                      return t;
                    })
                  );
                }}
              />
              <button
                onClick={() => {
                  setTaskList(taskList.filter((t) => t.id !== taskItem.id));
                }}
              >
                🗑️
              </button>
              <button
                onClick={() => {
                  setTaskList(
                    taskList.map((t) => {
                      if (t.id === taskItem.id) {
                        t.id = taskItem.id;
                        t.title = taskItem.title;
                        t.completed = !t.completed;
                      }
                      return t;
                    })
                  );
                }}
              >
                {taskItem.completed ? "To do" : "C'est fait !"}
              </button>
            </li>
          ))}
        </ul>

        {/* BOUTON REINITIALISER */}

        <button
          onClick={() => {
            localStorage.clear();
            setTaskList([]);
          }}
        >
          Réinitialiser
        </button>
      </div>
    );
  }
}

export default App;
